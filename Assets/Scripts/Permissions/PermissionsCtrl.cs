﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

#if PLATFORM_ANDROID
using UnityEngine.Android;
#endif
#if PLATFORM_IOS
using UnityEngine.iOS;
#endif

public class PermissionsCtrl : MonoBehaviour
{
    string[] AndroidPermissions;
    public static PermissionsCtrl Permissions;
    public bool HasPermissions;

    void Awake()
    {
        if (Permissions == null)
            Permissions = this;
        //"android.permission.READ_EXTERNAL_STORAGE",
        AndroidPermissions = new string[]
        {
            "android.permission.CAMERA",
        };
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    #region Check permissions
    public void CheckPermissions(Action Allowed, Action Denied)
    {
        #if !UNITY_EDITOR && UNITY_ANDROID
            AndroidRuntimePermissions.Permission[] permissions = AndroidRuntimePermissions.CheckPermissions(AndroidPermissions);
            if (IsGranted(permissions))
            {
                Allowed();
            }
            else
            {
                Denied();
            }
        #else
            Allowed();
        #endif
    }
    #endregion

    public bool CheckPermissions()
    {   

        #if PLATFORM_IOS
            return Application.HasUserAuthorization(UserAuthorization.WebCam)
        #endif
        #if PLATFORM_ANDROID
            AndroidRuntimePermissions.Permission[] permissions = AndroidRuntimePermissions.CheckPermissions(AndroidPermissions);
            if (IsGranted(permissions))
            {
                return true;
            }
            else
            {
                return false;
            }
        #endif
    }

    #region Request Permission
    public void RequestPermissions(Action Allowed, Action Denied)
    {
        #if PLATFORM_ANDROID
            AndroidRuntimePermissions.Permission[] permissions = AndroidRuntimePermissions.RequestPermissions(AndroidPermissions);
            if (IsGranted(permissions))
            {
                Allowed();
            }
            else
            {
                Denied();
            }
        #endif
        #if PLATFORM_IOS
            StartCoroutine(RequestIOSPermission(Allowed,Denied));
        #endif
    }

    IEnumerator RequestIOSPermission(Action Allowed, Action Denied){

        bool usedAccepted = true;
        yield return Application.RequestUserAuthorization(UserAuthorization.WebCam);
        if (!Application.HasUserAuthorization(UserAuthorization.WebCam))
            usedAccepted = false;
            
        if(usedAccepted){
            Allowed();
        }else{
            Denied();
        }
    }
    #endregion

    #region Check if is granted
    bool IsGranted(AndroidRuntimePermissions.Permission permission)
    {
        if (permission == AndroidRuntimePermissions.Permission.Granted)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    bool IsGranted(AndroidRuntimePermissions.Permission[] permissions)
    {
        bool isgranted = true;
        int size = permissions.Length;
        for (int i = 0; i < size; i++)
        {
            if (permissions[i] != AndroidRuntimePermissions.Permission.Granted)
            {
                isgranted = false;
            }
        }
        return isgranted;
    }
    #endregion


    public void OnRequestPermissions()
    {
        RequestPermissions(PermissionGranted, PermissionDenied);
    }

    public void OnCheckPermissions()
    {
        CheckPermissions(PermissionGranted, PermissionDenied);
    }

    void PermissionGranted()
    {
        Debug.Log("Allow permission");
    }

    public void PermissionDenied()
    {
        Debug.Log("Deny permission");
    }
}
