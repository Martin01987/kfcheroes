﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireManager : MonoBehaviour
{

    public float initialLife = 100;
    public float life;
    public Transform particles;

    [System.NonSerialized]
    public SpawnPositionManager spawnPosition;

    float initialScale = 0f;

    void OnEnable(){
        ActivateFire();
    }

    public void ActivateFire(){
        life = initialLife;
        if(initialScale == 0f)
            initialScale = particles.transform.localScale.x;
        particles.transform.localScale = new Vector3(initialScale,initialScale,initialScale);
    }

    public void AttackFire(float points){
        life-= points;

        float scaleToUse = (life * particles.transform.localScale.x)/100;
        particles.transform.localScale = new Vector3(scaleToUse, scaleToUse, scaleToUse);

        //Debug.Log(scaleToUse + " scaleToUse");
        if(scaleToUse <= 0.1f){
            SoundManager.instance.Play("steam");
            PoolManager.instance.DeactivateFire(transform);
            GameObject smokeParticles = PoolManager.instance.GetSmokeParticles();
            smokeParticles.transform.position = transform.position;
            smokeParticles.transform.rotation = transform.rotation;
            smokeParticles.GetComponent<ParticleSystem>().Play();
            BallsShooter.instance.UpdateFire(1);
            GameUIManager.instance.SetTextParticles("+1 apagado",Camera.main.WorldToScreenPoint(transform.position));
        }
            
    }
}
