﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RampGameManager : MonoBehaviour
{
    [Header("Game Assets")]
    public Rigidbody car;
    public GameObject carPositioner;
    public List<Transform> rampList = new List<Transform>();
    public Transform particles;
    public Transform diamond;
    
    [Header("Game Variables")]
    public bool isGoing;
    public float carSpeed = 20f;

    [Header("Game UI")]
    public CanvasGroup whiteFade;

    public static RampGameManager instance;
    
    [System.NonSerialized]
    public GameObject selectedObject;

    [System.NonSerialized]
    public bool isOnEditor;
    // Start is called before the first frame update

    void Awake(){
        Screen.orientation = ScreenOrientation.Landscape;
        isOnEditor = Application.isEditor;
    }

    void Start()
    {
        instance = this;
        isGoing = false;
        car.gameObject.SetActive(false);
        //InitializeGame();
    }

    public void SetWhiteFade(){
        whiteFade.gameObject.SetActive(true);
        whiteFade.alpha = 1;
        UIAnimation.FadeOutScreen(whiteFade,1f);
    }

    public void SetParticles(Transform objective){
        particles.position = objective.position;
        particles.gameObject.SetActive(true);
        particles.GetComponent<ParticleSystem>().Play();
    }

    public void InitializeCar(){
        car.transform.position = new Vector3(carPositioner.transform.position.x, carPositioner.transform.position.y +0.05f, carPositioner.transform.position.z);
        car.transform.rotation = carPositioner.transform.rotation;
        car.gameObject.SetActive(true);
        car.velocity = Vector3.zero;
        car.angularVelocity = Vector3.zero;

    }

    public void InitializeGame(){
        SetWhiteFade();
        car.gameObject.SetActive(false);
        foreach(Transform ramp in rampList){
            ramp.transform.localPosition = new Vector3(0,0,0);
        }
        diamond.localPosition = new Vector3(Random.Range(-0.3f,0.4f),diamond.localPosition.y,Random.Range(-0.2f,0.2f));
        diamond.gameObject.SetActive(true);

        carPositioner.transform.localPosition = new Vector3(0,0,-0.2f);
    }

    public void StartGame(){

        InitializeCar();
        isGoing = true;
        car.velocity = car.transform.forward * carSpeed;
        SoundManager.instance.Play("car_sound");
    }

    public void RestartGame(){
        Invoke("InitializeGame",2f);
    }

    // Update is called once per frame
    void Update()
    {
        if(isGoing){
            //car.velocity = car.transform.forward * carSpeed;
        }
    }
}
