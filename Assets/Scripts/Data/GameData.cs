﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class GameData : MonoBehaviour
{
    //[System.NonSerialized]
    public int gameID;

    public static GameData instance;

    void Awake()
    {
        if(instance !=  null){
            Destroy(gameObject);
            return;
        }
        instance = this;
        this.enabled = false;
        DontDestroyOnLoad(this.gameObject);
    }

    /// <summary>
    /// Sets the id for the game type
    /// </summary>
    /// <param name="screen">0 = DodgeBall, 1 = Chomp Chomp, 2 = Talk, 3 = Photos</param>
    public void SetGameID(int id){
        gameID = id;
    }
    
}
