﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Snowball : MonoBehaviour
{
    public float speed = 1500;
    private Rigidbody rb;

    [System.NonSerialized]
    public BallsShooter ballsShooter;
    private bool lite;
    bool hasCollide;
    Coroutine disappearCoroutine;
    void Awake()
    {
        rb = this.GetComponent<Rigidbody>();
    }

    void OnEnable(){
        rb.AddRelativeForce(Vector3.forward *  speed);
        rb.AddForce(new Vector3(Random.Range(-50f,50f),speed * 0.1f,0));
        hasCollide = false;
        disappearCoroutine = StartCoroutine(WaitAndDisappear());
    }

    void OnTriggerEnter(Collider other){

        string tag = other.tag;
        if(tag == "Floor" || tag == "Fire" || tag == "Item"){

            if(tag == "Fire")
                other.GetComponent<FireManager>().AttackFire(1);
            if(tag == "Item"){
                ItemManager item = other.GetComponent<ItemManager>();
                Transform particles = null;
                switch(item.itemType){
                    case(ItemManager.ItemType.Chronometer):
                        SoundManager.instance.Play("item");
                        SoundManager.instance.Play("reloj");
                        GameUIManager.instance.UpdateTime(5);
                        particles = PoolManager.instance.GetStarParticles().transform;
                        GameUIManager.instance.SetTextParticles("+5 segundos",Camera.main.WorldToScreenPoint(transform.position));
                    break;
                    case(ItemManager.ItemType.Bomb):
                        SoundManager.instance.Play("bomb");
                        GameUIManager.instance.UpdateTime(-3);
                        particles = PoolManager.instance.GetExplosionParticles().transform;
                        GameUIManager.instance.SetTextParticles("-3 segundos",Camera.main.WorldToScreenPoint(transform.position));
                    break;
                }
                item.spawnPosition.hasFire = false;
                PoolManager.instance.DeactivateBall(other.transform);
                particles.transform.position = transform.position;
                particles.GetComponent<ParticleSystem>().Play();
            }
            ParticleSystem waterParticles = PoolManager.instance.GetWaterParticles().GetComponent<ParticleSystem>();
            waterParticles.transform.position = transform.position;
            waterParticles.transform.rotation = transform.rotation;
            waterParticles.Play();

            if(!hasCollide)
                DisappearSeconds();
        }
    }

    IEnumerator WaitAndDisappear(){

        yield return new WaitForSeconds(1.5f);
        DisappearSeconds();
    }

    void DisappearSeconds(){
        if(disappearCoroutine != null)
            StopCoroutine(disappearCoroutine);
        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;
        hasCollide = true;
        PoolManager.instance.DeactivateBall(transform);
    }
}
