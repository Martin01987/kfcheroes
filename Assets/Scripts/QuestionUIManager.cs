﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class Question{
    public string question;
    public List<string> options = new List<string>();
    public int correctOption;
}

public class QuestionUIManager : MonoBehaviour
{
    [Header("Question Assets")]
    public Text questionText;
    public List<Text> buttonTextOptions = new List<Text>();

    public Text resultText;

    [Header("Panels")]
    public RectTransform questionPanel;
    public RectTransform resultsPanel;
    public CanvasGroup searchUI;

    [Header("Questions")]
    public List<Question> questions = new List<Question>();
    bool isTracked = false;
    bool currentCorrect = false;

    int indexTracking;
    int indexQuestion;

    void Awake(){
        searchUI.gameObject.SetActive(true);
    }

    void Start()
    {
        indexQuestion = 0;
        indexTracking = 0;
        //TargetsManager.instance.DeactivateTargets(indexQuestion);
    }

    public void ShowQuestion(){

        if(searchUI.gameObject.activeSelf)
            UIAnimation.FadeOutScreen(searchUI,0.5f);
        Question currentQuestion = questions[indexQuestion];
        questionText.text = currentQuestion.question;

        for(int i = 0; i < questions.Count; i++){
            buttonTextOptions[i].text = currentQuestion.options[i];
        }
        UIAnimation.ShowVertical(questionPanel);
    }

    public void TargetFound(Transform target){

        indexTracking++;
        Debug.Log(indexTracking + " indexTracking");
        TargetBehaviour targetBehaviour = target.GetComponent<TargetBehaviour>();
        if(targetBehaviour.isTracked)
            return;
        if(indexQuestion == 0){
            if(targetBehaviour.isFirstBucket){
                TargetsManager.instance.SetTargets(TargetsManager.instance.FirtsBucketTargets);
            }else{
                TargetsManager.instance.SetTargets(TargetsManager.instance.SecondBucketTargets);
            }
            TargetsManager.instance.DeactivateAllTargets();
        }

        targetBehaviour.isTracked = true;
        target.gameObject.SetActive(false);
        GameObject starParticles = PoolManager.instance.GetStarParticles();
        starParticles.transform.position = target.position;
        starParticles.GetComponent<ParticleSystem>().Play();
        Invoke("ShowQuestion",1f);
    }

    public void AnswerQuestion(int index){
        currentCorrect = index == questions[indexQuestion].correctOption;
        UIAnimation.HideVertical(questionPanel);
        UIAnimation.ShowVertical(resultsPanel);
        if(currentCorrect){
            resultText.text = "CORRECTA";
            indexQuestion++;
            if(indexQuestion >= 3){
                resultText.text+= "¡HAZ GANADO!";
            }else{
                TargetsManager.instance.DeactivateTargets(indexQuestion);
            }
        }else{
            resultText.text = "INCORRECTA";
        } 
    }

    public void HideResultsUI(){
        UIAnimation.HideVertical(resultsPanel);
    }
}
