﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunnerPoolManager : MonoBehaviour
{
    [System.NonSerialized]
    public List<GameObject> tileList = new List<GameObject>();
    [Header("Tile Assets")]
    public GameObject tilePrefab;
    Transform lastTile;

    public Transform poolParent;
    public static RunnerPoolManager instance;

    [Header("Item Assets")]
    public GameObject rockPrefab;
    public GameObject boxPrefab;
    public GameObject chickenPrefab;

    [Header("Particles")]
    public GameObject smokeParticlesPrefab;
    public GameObject starParticlesPrefab;

    List<GameObject> starParticlesList = new List<GameObject>();
    List<GameObject> smokeParticlesList = new List<GameObject>();

    public GameObject textParticlesPrefab;
    List<GameObject> textParticlesList = new List<GameObject>();

    [Header("Game Variables")]
    public float[] positions;

    [System.NonSerialized]
    public List<GameObject> itemList = new List<GameObject>();
    Transform lastItem;

    void Awake(){
        instance = this;
    }

    void Start(){
        AddTile(5);
    }

    public void DeactivateTile(Transform tile){
        tile.gameObject.SetActive(false);
        tile.SetParent(poolParent);
    }

    public void AddTile(int tilesNumber){

        for(var i = 0; i <tilesNumber;i++){
            GameObject tileUsed = null;
            foreach (GameObject item in tileList){
                if(!item.activeSelf){
                    tileUsed = item;
                    break;
                }
            }
            if(tileUsed == null){
                tileUsed = Instantiate(tilePrefab); 
                tileList.Add(tileUsed);
            }
            tileUsed.SetActive(true);
            if(lastTile != null){
                tileUsed.transform.position = new Vector3(lastTile.transform.position.x + 19.2f, lastTile.transform.position.y, lastTile.transform.position.z);
            }
            lastTile = tileUsed.transform;
        }
    }

    public void GetItem(string tag){

        GameObject itemUsed = null;
        foreach (GameObject item in itemList){
            if(!item.activeSelf && item.tag == tag){
                itemUsed = item;
                break;
            }
        }
        if(itemUsed == null){
            if(tag == "BoxObstacle"){
                itemUsed = Instantiate(boxPrefab); 
            }else if(tag == "Chicken"){
                itemUsed = Instantiate(chickenPrefab); 
            }else if(tag == "RockObstacle"){
                itemUsed = Instantiate(rockPrefab); 
            }
            
            itemList.Add(itemUsed);
        }
        itemUsed.SetActive(true);
        itemUsed.transform.position = new Vector3(14f,itemUsed.transform.position.y, positions[Random.Range(0,positions.Length)]);
    }

    public void DeactivateItem(Transform item){
        item.gameObject.SetActive(false);
        item.SetParent(poolParent);
    }

    public GameObject GetSmokeParticles(){
        GameObject particlesUsed = null;
        foreach (GameObject smokeParticle in smokeParticlesList){
            if(!smokeParticle.activeSelf){
                particlesUsed = smokeParticle;
                break;
            }
        }
        if(particlesUsed == null){
            particlesUsed = Instantiate(smokeParticlesPrefab,transform.position,transform.rotation); 
            smokeParticlesList.Add(particlesUsed);
        }

        particlesUsed.gameObject.SetActive(true);
        StartCoroutine(RecycleParticles(particlesUsed.transform,2f));
        return particlesUsed;
    }

    public GameObject GetStarParticles(){
        GameObject particlesUsed = null;
        foreach (GameObject starParticle in starParticlesList){
            if(!starParticle.activeSelf){
                particlesUsed = starParticle;
                break;
            }
        }
        if(particlesUsed == null){
            particlesUsed = Instantiate(starParticlesPrefab,transform.position,transform.rotation); 
            starParticlesList.Add(particlesUsed);
        }
        particlesUsed.SetActive(true);
        StartCoroutine(RecycleParticles(particlesUsed.transform,0.5f));
        return particlesUsed;
    }

    IEnumerator RecycleParticles(Transform particles,float time){
        yield return new WaitForSeconds(time);
        particles.gameObject.SetActive(false);
        particles.SetParent(poolParent);
    }

    public GameObject GetTextParticles(){
        GameObject particlesUsed = null;
        foreach (GameObject textParticle in textParticlesList){
            if(!textParticle.activeSelf){
                particlesUsed = textParticle;
                break;
            }
        }
        if(particlesUsed == null){
            particlesUsed = Instantiate(textParticlesPrefab,transform.position,transform.rotation); 
            textParticlesList.Add(particlesUsed);
        }
        particlesUsed.SetActive(true);
        particlesUsed.GetComponent<RectTransform>().SetParent(RunnerUIManager.instance.transform);
        StartCoroutine(RecycleParticles(particlesUsed.transform,0.5f));
        return particlesUsed;
    }
}
