﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Simulates the player movement inUnity Editor, this script is only for testing
/// </summary>
public class PlayerSimulatorCtrl : MonoBehaviour
{
    public bool IsOnEditor;

    [Header ("Rotation Setup")]
    public float VerticalSpeed;
    public float HorizontalSpeed;
    public float Yaw;
    public float Pitch;

    [Header ("Movement Setup")]
    public float Speed;

    [Header ("Camera")]
    public Transform CameraTransform;
    // Start is called before the first frame update
    void Start()
    {
        CameraTransform = Camera.main.transform;
        IsOnEditor = Application.isEditor;
    }

    // Update is called once per frame
    void Update()
    {
        if(IsOnEditor)
        {
            RotateCamera();
            UpdateCharacterPosition();
        }
    }

    public void RotateCamera()
    {
        Yaw += HorizontalSpeed * Input.GetAxis("Mouse X");
        Pitch -= VerticalSpeed * Input.GetAxis("Mouse Y");

        CameraTransform.eulerAngles = new Vector3(Pitch, Yaw, 0.0f);
    }

    public void UpdateCharacterPosition()
    {
        float x = Input.GetAxis ("Horizontal") * Time.deltaTime * Speed;
        float z = Input.GetAxis ("Vertical") * Time.deltaTime * Speed;
 
        // Translate the object
        CameraTransform.Translate (x , 0, z);
    }
}
