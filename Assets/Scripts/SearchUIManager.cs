﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SearchUIManager : MonoBehaviour
{

    public Text searchText;
    private int indexCount = 0;

    void OnEnable(){
        StartCoroutine(ChangeText());
    }

    IEnumerator ChangeText(){
        indexCount++;
        if(indexCount >3)
            indexCount = 0;
        searchText.text = "BUSCANDO";
        for(int i = 0; i < indexCount; i++){
            searchText.text+=".";
        }
        yield return new WaitForSeconds(0.5f);
        StartCoroutine(ChangeText());
    }
}
