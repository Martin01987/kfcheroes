﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolManager : MonoBehaviour
{
    
    public static PoolManager instance;
    public Transform poolParent;

    [Header("Ball Assets")]
    public GameObject ballPrefab;
    List<GameObject> ballList = new List<GameObject>();

    [Header("Particles Assets")]
    public GameObject waterParticlesPrefab;
    List<GameObject> waterParticlesList = new List<GameObject>();

    public GameObject smokeParticlesPrefab;
    List<GameObject> smokeParticlesList = new List<GameObject>();

    public GameObject starParticlesPrefab;
    List<GameObject> starParticlesList = new List<GameObject>();

    public GameObject textParticlesPrefab;
    List<GameObject> textParticlesList = new List<GameObject>();

    public GameObject explosionParticlesPrefab;
    List<GameObject> explosionParticlesList = new List<GameObject>();

    [Header("Fire Asset")]
    public GameObject firePrefab;
    List<GameObject> fireList = new List<GameObject>();

    [Header("Items")]
    public GameObject itemPrefab;
    List<GameObject> itemList = new List<GameObject>();
    public GameObject bombPrefab;
    List<GameObject> bombList = new List<GameObject>();

    void Awake()
    {
        instance = this;
    }

    public GameObject GetBall(){

        GameObject ballUsed = null;
        foreach (GameObject ball in ballList){
            if(!ball.activeSelf){
                ballUsed = ball;
                break;
            }
        }
        if(ballUsed == null){
            ballUsed = Instantiate(ballPrefab,transform.position,transform.rotation); 
            ballList.Add(ballUsed);
        }
        return ballUsed;
    }

    public void DeactivateBall(Transform ball){
        ball.gameObject.SetActive(false);
        ball.SetParent(poolParent);
    }

    public GameObject GetItem(){

        GameObject itemUsed = null;
        foreach (GameObject item in itemList){
            if(!item.activeSelf){
                itemUsed = item;
                break;
            }
        }
        if(itemUsed == null){
            itemUsed = Instantiate(itemPrefab); 
            itemList.Add(itemUsed);
        }
        itemUsed.SetActive(true);
        return itemUsed;
    }

    public GameObject GetBomb(){

        GameObject bombUsed = null;
        foreach (GameObject bomb in bombList){
            if(!bomb.activeSelf){
                bombUsed = bomb;
                break;
            }
        }
        if(bombUsed == null){
            bombUsed = Instantiate(bombPrefab); 
            bombList.Add(bombUsed);
        }
        bombUsed.SetActive(true);
        return bombUsed;
    }

    public GameObject GetFire(){

        GameObject fireUsed = null;
        foreach (GameObject fire in fireList){
            if(!fire.activeSelf){
                fireUsed = fire;
                break;
            }
        }
        if(fireUsed == null){
            fireUsed = Instantiate(firePrefab); 
            fireList.Add(fireUsed);
        }
        fireUsed.SetActive(true);
        return fireUsed;
    }

    public void DeactivateFire(Transform fire){
        fire.GetComponent<FireManager>().spawnPosition.hasFire = false;
        fire.gameObject.SetActive(false);
        fire.SetParent(poolParent);
    }

    public GameObject GetWaterParticles(){
        GameObject particlesUsed = null;
        foreach (GameObject waterParticle in waterParticlesList){
            if(!waterParticle.activeSelf){
                particlesUsed = waterParticle;
                break;
            }
        }
        if(particlesUsed == null){
            particlesUsed = Instantiate(waterParticlesPrefab,transform.position,transform.rotation); 
            waterParticlesList.Add(particlesUsed);
        }
        particlesUsed.SetActive(true);
        StartCoroutine(RecycleParticles(particlesUsed.transform,0.5f));
        return particlesUsed;
    }

    public GameObject GetExplosionParticles(){
        GameObject particlesUsed = null;
        foreach (GameObject explosionParticle in explosionParticlesList){
            if(!explosionParticle.activeSelf){
                particlesUsed = explosionParticle;
                break;
            }
        }
        if(particlesUsed == null){
            particlesUsed = Instantiate(explosionParticlesPrefab,transform.position,transform.rotation); 
            explosionParticlesList.Add(particlesUsed);
        }
        particlesUsed.SetActive(true);
        StartCoroutine(RecycleParticles(particlesUsed.transform,2f));
        return particlesUsed;
    }

    public GameObject GetTextParticles(){
        GameObject particlesUsed = null;
        foreach (GameObject textParticle in textParticlesList){
            if(!textParticle.activeSelf){
                particlesUsed = textParticle;
                break;
            }
        }
        if(particlesUsed == null){
            particlesUsed = Instantiate(textParticlesPrefab,transform.position,transform.rotation); 
            textParticlesList.Add(particlesUsed);
        }
        particlesUsed.SetActive(true);
        particlesUsed.GetComponent<RectTransform>().SetParent(GameUIManager.instance.transform);
        StartCoroutine(RecycleParticles(particlesUsed.transform,0.5f));
        return particlesUsed;
    }

    public GameObject GetSmokeParticles(){
        GameObject particlesUsed = null;
        foreach (GameObject smokeParticle in smokeParticlesList){
            if(!smokeParticle.activeSelf){
                particlesUsed = smokeParticle;
                break;
            }
        }
        if(particlesUsed == null){
            particlesUsed = Instantiate(smokeParticlesPrefab,transform.position,transform.rotation); 
            smokeParticlesList.Add(particlesUsed);
        }

        particlesUsed.gameObject.SetActive(true);
        StartCoroutine(RecycleParticles(particlesUsed.transform,2f));
        return particlesUsed;
    }

    public GameObject GetStarParticles(){
        GameObject particlesUsed = null;
        foreach (GameObject starParticle in starParticlesList){
            if(!starParticle.activeSelf){
                particlesUsed = starParticle;
                break;
            }
        }
        if(particlesUsed == null){
            particlesUsed = Instantiate(starParticlesPrefab,transform.position,transform.rotation); 
            starParticlesList.Add(particlesUsed);
        }
        particlesUsed.SetActive(true);
        StartCoroutine(RecycleParticles(particlesUsed.transform,0.5f));
        return particlesUsed;
    }

    IEnumerator RecycleParticles(Transform particles,float time){
        yield return new WaitForSeconds(time);
        particles.gameObject.SetActive(false);
        particles.SetParent(poolParent);
    }

    public void RestartAssets(){
        foreach(GameObject fire in fireList){
            fire.GetComponent<FireManager>().spawnPosition.hasFire = false;
            DeactivateFire(fire.transform);
        }

        foreach(GameObject item in itemList){
            item.GetComponent<ItemManager>().spawnPosition.hasFire = false;
            DeactivateBall(item.transform);
        }

        foreach(GameObject bomb in bombList){
            bomb.GetComponent<ItemManager>().spawnPosition.hasFire = false;
            DeactivateBall(bomb.transform);
        }
    }
}
