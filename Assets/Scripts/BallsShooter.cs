﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class BallsShooter : MonoBehaviour
{
    public Transform shootPivot;
    public static BallsShooter instance;

    private GameObject cameraSnow;

    [Header("Game Assets")]
    public GameObject hose;
    public GameObject ARTarget;
    public GameObject ARTarget2;
    public GameObject NonARTarget;

    [Header("Positions")]
    public Transform parentPositions;
    List<SpawnPositionManager> positions = new List<SpawnPositionManager>();

    [System.NonSerialized]
    public int ballsShot = 0;

    [Header("Game Variables")]
    public float timeToSpawn = 3f;
    public int firesNumber = 10;
    public Coroutine spawnCoroutine;

    [System.NonSerialized]
    public int actualFires;
    private bool shootReady = false;
    private bool waterSoundActive = true;
    public bool isPlaying = false;
    private bool isTracking;
    bool isOnEditor;
    Animator hoseAnimator;

    void Awake(){
        isTracking = true;
        instance = this;
        Screen.orientation = ScreenOrientation.Portrait;
        actualFires = 0;
        
    }

    public void UpdateFire(int number){
        actualFires+= number;
        if(actualFires >= firesNumber){
            GameUIManager.instance.ShowScreen(GameUIManager.instance.winScreen);
            LoseGame();
            SoundManager.instance.Play("ganaste");
        }
        GameUIManager.instance.UpdateFireText(actualFires,firesNumber); 

    }
    
    public void ActivateGame(bool active){
        hose.SetActive(active);
        shootReady = active;
        if(active){
            isPlaying = true;
            hoseAnimator.enabled = false;
            UIAnimation.PopImage(hose.transform,0.3f);
            StartCoroutine(SpawnFire());
            SoundManager.instance.Play("cut");
        }   
    }

    IEnumerator SpawnFire(){

        Transform position = GetPosition();
        if(position != null){   
            SpawnPositionManager spawnPosition = position.GetComponent<SpawnPositionManager>();
            spawnPosition.hasFire = true;
            GameObject item;       
            if(Random.Range(0,10) > 6){
                if(Random.Range(0,10)>5){
                    item = PoolManager.instance.GetBomb();
                    SoundManager.instance.Play("wrong");
                }else{
                    item = PoolManager.instance.GetItem();
                    SoundManager.instance.Play("pop");
                }
                item.transform.localScale = new Vector3(1,1,1);
                UIAnimation.PopImage(item.transform,0.2f);
                //item.transform.localRotation = Quaternion.Euler(-15f,0,0);
                item.GetComponent<ItemManager>().spawnPosition = spawnPosition;
            }else{
                item = PoolManager.instance.GetFire();
                SoundManager.instance.Play("fire_sound");
                item.GetComponent<FireManager>().spawnPosition = spawnPosition;
                //item.transform.rotation = Quaternion.Euler(-60,0,0);
            }   
            item.transform.position = position.position;
            item.transform.SetParent(position);
            Debug.Log(item.transform.localRotation);
        }
        yield return new WaitForSeconds(timeToSpawn);
        spawnCoroutine = StartCoroutine(SpawnFire());
    }

    Transform GetPosition(){

        Transform usedPosition = null;
        List<SpawnPositionManager> availablePositions = new List <SpawnPositionManager>();
        foreach(SpawnPositionManager position in positions){
            if(!position.hasFire)
                availablePositions.Add(position);
        }
        if(availablePositions.Count > 0)
            usedPosition = availablePositions[Random.Range(0,availablePositions.Count)].transform;
        return usedPosition;
    }

    public void CheckARObjects(GameObject ARObject){

        if(!isTracking)
            return;
        isTracking = false;
        ARTarget.SetActive(false);
        ARTarget2.SetActive(false);

        ARObject.SetActive(true);
        SetPositions(ARObject.transform);
    }

    public void SetPositions(Transform transform){

        parentPositions = transform;
        GameUIManager.instance.UpdateFireText(actualFires,firesNumber);
        foreach(Transform child in parentPositions){
            if(child.GetComponent<SpawnPositionManager>() != null)
                positions.Add(child.GetComponent<SpawnPositionManager>());
        }
    }

    void Start(){
        isOnEditor = Application.isEditor;
        if(isOnEditor){
            SetPositions(NonARTarget.transform);
        }

        hoseAnimator = hose.GetComponent<Animator>();
        ActivateGame(false);

        
        ARTarget.SetActive(!isOnEditor);
        NonARTarget.SetActive(isOnEditor);

        if(isOnEditor)
            GameUIManager.instance.ShowInstructions();
    }

    void Update ()
    {
        if (Input.GetMouseButton(0))
            ShootBall();

        if(Input.GetMouseButtonUp(0))
            hoseAnimator.Play("Idle");
    }

    IEnumerator DeactivateActivate(){
        shootReady = false;
        yield return new WaitForSeconds(0.07f);
        shootReady = true;
    }

    IEnumerator DeactivateSound(){
        waterSoundActive = false;
        yield return new WaitForSeconds(0.5f);
        waterSoundActive = true;
    }

    void ShootBall(){

        if(!shootReady || !isPlaying)
            return;
        if(!hoseAnimator.enabled)
            hoseAnimator.enabled = true;
        hoseAnimator.Play("HoseAnimation");
        if(waterSoundActive){
            SoundManager.instance.Play("water");
            StartCoroutine(DeactivateSound());
        }
        StartCoroutine("DeactivateActivate");
        GameObject ballUsed = PoolManager.instance.GetBall();
        
        ballUsed.transform.parent = null;
        ballUsed.transform.position = shootPivot.position;
        ballUsed.transform.rotation = shootPivot.rotation;
        
        float scaleToUse = Random.Range(0.3f,0.6f);
        ballUsed.transform.localScale = new Vector3(scaleToUse,scaleToUse,scaleToUse);
        ballUsed.SetActive(true);
    }

    public void LoseGame(){
        SoundManager.instance.StopMusic();
        isPlaying = false;
        StopCoroutine(spawnCoroutine);
    }

}
