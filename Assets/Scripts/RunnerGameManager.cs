﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class RunnerGameManager : MonoBehaviour
{
    [Header("Character Assets")]
    public Transform character;
    public Transform characterPivot;
    public float moveValue = 3f;
    public float timeMove = 0.2f;

    [Header("Game Values")]
    public float gameSpeed = 1.0f;
    public bool isMoving = true;
    public float initPos = -10f;
    public float timeSpan = 1f;
    public int playerLives = 5;

    [System.NonSerialized]
    public bool canMove = false;

    public static RunnerGameManager instance;
    void Start()
    {
        instance = this;
        Screen.orientation = ScreenOrientation.Landscape;
        SoundManager.instance.PlayMusic("musica_fondo_runner");
    }

    public void MovePlayer(bool isUp){
        if(!canMove)
            return;
        canMove = false;
        float value = moveValue;
        if(isUp)
            value*= -1;

        float finalPos = character.position.z + value;
        if(finalPos > moveValue || finalPos< -moveValue){
            canMove = true;
            return;
        }
            
        character.DOMoveZ(finalPos,timeMove).OnComplete(()=>{
            canMove = true;
        }).SetEase(Ease.OutBack);
    }

    public void StartGame(){
        gameSpeed*= 2.5f;
        character.gameObject.SetActive(true);

        RunnerCharacterBehaviour characterBehaviour = character.GetComponent<RunnerCharacterBehaviour>();
        characterBehaviour.selecting = false;
        characterBehaviour.characterContainer.localPosition = new Vector3(1.13f,0,0);
        character.position = characterPivot.position;
        character.rotation = characterPivot.rotation;
        character.transform.DOMoveX(character.position.x - 20,1f).From().SetEase(Ease.OutBack).OnComplete(()=>{
            canMove = true;
        });

        StartCoroutine("SendItem");
    }

    IEnumerator SendItem(){
        yield return new WaitForSeconds(timeSpan);
        string obstacleName;
        if(Random.Range(0,10) > 8){
            obstacleName = "Chicken";
        }else{
            List<string> obstaclesList = new List<string>{"BoxObstacle","RockObstacle"};
            obstacleName = obstaclesList[Random.Range(0,obstaclesList.Count)];
        }
        
        RunnerPoolManager.instance.GetItem(obstacleName);
        StartCoroutine("SendItem");
    }

    public void StopGame(){
        isMoving = false;
        canMove = false;
    }

    void Update(){

        if(isMoving){
            for(var i = 0; i < RunnerPoolManager.instance.tileList.Count;i++){
                Transform tile = RunnerPoolManager.instance.tileList[i].transform;
                if(tile.gameObject.activeSelf){
                    tile.Translate(-gameSpeed * Time.deltaTime, 0,0);
                    if(tile.position.x <= initPos){
                        RunnerPoolManager.instance.DeactivateTile(tile);
                        RunnerPoolManager.instance.AddTile(1);
                    }
                }
            }

            for(var i = 0; i < RunnerPoolManager.instance.itemList.Count;i++){
                Transform item = RunnerPoolManager.instance.itemList[i].transform;
                if(item.gameObject.activeSelf){
                    item.Translate(-gameSpeed * Time.deltaTime,0,0);
                    if(item.position.x <= character.position.x -10){
                        RunnerPoolManager.instance.DeactivateItem(item);
                    }
                }
            }
        }
    }
}
