﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class RunnerCharacterBehaviour : MonoBehaviour
{
    [Header("Assets")]
    public Transform[] vehicles;
    public Transform[] hats;
    public Transform characterContainer;
    public Animator characterAnim;

    [Header("Materials")]
    public Material suitMaterial;
    public Material headMaterial;

    [Header("Textures")]
    public Texture[] suitTextures;

    public bool selecting = true;
    public bool canHit = true;

    void Update(){
        if(selecting)
            transform.Rotate(0,-2,0);
    }

    public IEnumerator PlayerCrash(){

        if(canHit){
            RunnerUIManager.instance.UpdateBar(-1);
            characterAnim.Play("Crash");
            canHit = false;
            /*int index = 0;
            while(index<4){
                characterContainer.gameObject.SetActive(false);
                yield return new WaitForSeconds(0.1f);
                characterContainer.gameObject.SetActive(true);
                yield return new WaitForSeconds(0.1f);
                index++;
            }*/
            yield return new WaitForSeconds(0.8f);
            canHit = true;
        }
    }

    void OnTriggerEnter(Collider other){

        GameObject particles = null;
        if(other.CompareTag("BoxObstacle") || other.CompareTag("RockObstacle")){
            particles = RunnerPoolManager.instance.GetSmokeParticles();
            RunnerUIManager.instance.SetTextParticles("-1",Camera.main.WorldToScreenPoint(other.transform.position));
            StartCoroutine(PlayerCrash());
        }else if(other.CompareTag("Chicken")){
            particles = RunnerPoolManager.instance.GetStarParticles();
            RunnerUIManager.instance.SetTextParticles("+1",Camera.main.WorldToScreenPoint(other.transform.position));
            RunnerUIManager.instance.UpdateChicken(1);
            characterAnim.Play("Win");
        }

        if(other.CompareTag("BoxObstacle") || other.CompareTag("RockObstacle") || other.CompareTag("Chicken")){
            particles.transform.position = other.transform.position;
            particles.GetComponent<ParticleSystem>().Play();
            RunnerPoolManager.instance.DeactivateItem(other.transform);
        }
    }
}
