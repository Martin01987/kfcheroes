﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemManager : MonoBehaviour
{

    public enum ItemType {Chronometer,Bomb};
    public ItemType itemType;

    [System.NonSerialized]
    public SpawnPositionManager spawnPosition;

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(0,5,0);
    }

    void OnEnable(){
        switch(itemType){
            case ItemType.Bomb:
                StartCoroutine(HideBomb());
            break;
        }
    }

    IEnumerator HideBomb(){
        yield return new WaitForSeconds(3f);
        UIAnimation.HideScaleDown(transform,1f);
    }
}
