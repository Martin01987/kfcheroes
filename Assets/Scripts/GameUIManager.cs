﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameUIManager : MonoBehaviour
{

    [System.NonSerialized]
    public bool UIActive;
    public static GameUIManager instance;

    [Header("Panels")]
    public RectTransform instructionsScreen;
    public RectTransform loseScreen;
    public RectTransform winScreen;
    public RectTransform tutorialScreen;
    public RectTransform exitScreen;
    public CanvasGroup searchGUI;
    public CanvasGroup counterGUI;    
    public RectTransform timeUI;
    public RectTransform fireUI;
    public CanvasGroup whiteFade;

    [Header("UI Texts")]
    public Text fireUIText;
    public Text timeText;

    [Header("Game Functionality")]
    public BallsShooter ballsShooter;
    public int initialTime;
    [System.NonSerialized]
    public int timeUsed;
    public ButtonManager backButton;

    [Header("Tutorial Assets")]
    public List<CanvasGroup> tutorialScreens;

    int tutorialStep;
    CanvasGroup currentBackScreen;

    [System.NonSerialized]
    public RectTransform actualScreen;
    [System.NonSerialized]
    public bool isTracked = false;

    Text counterGUIText;
    // Start is called before the first frame update
    void Awake()
    {   
        isTracked = false;
        instance = this;
        counterGUIText = counterGUI.GetComponentInChildren<Text>();
        searchGUI.gameObject.SetActive(true);
        ActivateUI(false);

        SoundManager.instance.PlayMusic("musica_fondo_bucket_llamas");
    }

    void ActivateUI(bool active){

        fireUI.gameObject.SetActive(active);
        timeUI.gameObject.SetActive(active);

        if(active){
            UIAnimation.PopImage(fireUI,0.2f,0.5f);
            UIAnimation.PopImage(timeUI,0.2f);
            SoundManager.instance.Play("cronometro");
        }
        
    }

    public void ShowExitScreen(bool active){

        if(active){
            ShowScreen(exitScreen);
            UIAnimation.FadeOutButton(backButton);
            currentBackScreen = null;
            if(instructionsScreen.gameObject.activeSelf){
                currentBackScreen = instructionsScreen.GetComponent<CanvasGroup>();
            }else if(tutorialScreen.gameObject.activeSelf){
                currentBackScreen = tutorialScreen.GetComponent<CanvasGroup>();
            }
            if(currentBackScreen != null)
                UIAnimation.FadeOutScreen(currentBackScreen,0.5f);
        }else{
            HideScreen(exitScreen);
            UIAnimation.FadeInButton(backButton);
            if(currentBackScreen != null)
                UIAnimation.FadeInScreen(currentBackScreen,0.5f,0.5f);
        }
    }

    public void BackToMenu(){
        ScreenWipe.instance.nextScene = "MainMenu";
        StartCoroutine(ScreenWipe.instance.CrossFadeToScene("LoadingScene",1f));
    }

    public void SetWhiteFade(){
        whiteFade.gameObject.SetActive(true);
        whiteFade.alpha = 1;
        UIAnimation.FadeOutScreen(whiteFade,0.5f);
    }

    public void SetTextParticles(string text,Vector3 position){

        Text textPart = PoolManager.instance.GetTextParticles().GetComponent<Text>();
        textPart.rectTransform.localScale = new Vector3(0.8f,0.8f,0.8f);
        textPart.text = text;
        textPart.rectTransform.position = position;
        UIAnimation.ShowFade(textPart.rectTransform);
    }

    public void SetTextParticles(string text, RectTransform transform){
        SetTextParticles(text, transform.position);
    }

    public void ShowInstructions(){
        if(isTracked)
            return;
        UIAnimation.FadeOutScreen(searchGUI,0.5f);
        isTracked = true;
        ShowScreen(instructionsScreen);
    }

    public void ShowScreen(RectTransform screen){
        UIAnimation.ShowVertical(screen);
        actualScreen = screen;
    }

    public void HideScreen(RectTransform screen){
        UIAnimation.HideVertical(screen);
    }

    public void StartCount(){
        Invoke("StartCounting",1f);
    }

    void StartCounting(){
        StartCoroutine(StartCounter(3));
    }

    IEnumerator StartCounter(int indexCount){

        counterGUIText.text = ""+indexCount;
        UIAnimation.PopImage(counterGUI.transform,0.1f);
        counterGUI.alpha = 1;
        UIAnimation.FadeOutScreen(counterGUI,0.2f,0.5f);
        SoundManager.instance.Play("conteo");
        yield return new WaitForSeconds(1f);

        if(!UIActive)
            indexCount--;
        if(indexCount <= 0){
            timeUsed = initialTime;
            ballsShooter.ActivateGame(true);
            StartCoroutine(StartTimer());
            ActivateUI(true);
        }else{
            StartCoroutine(StartCounter(indexCount));
        }
    }

    public void UpdateTime(int value){
        if(!ballsShooter.isPlaying)
            return;
        timeUsed+= value;
        if(timeUsed < 0)
            timeUsed = 0;
        timeText.text = timeUsed+"";
    


        string sign = "";
        if(value > 0)
            sign = "+";
        SetTextParticles(sign+value,timeText.rectTransform);
        
    }

    IEnumerator StartTimer(){

        UpdateTime(-1);
        UIAnimation.PopImage(timeText.transform.parent,0.5f);
        yield return new WaitForSeconds(1f);
        if(ballsShooter.isPlaying){
            if(timeUsed <= 0){
                LoseGame();
            }else{
                StartCoroutine(StartTimer());
            }
        }
            
    }

    public void LoseGame(){
        ballsShooter.LoseGame();
        ShowScreen(loseScreen);
    }

    public void RestartGame(){
        SoundManager.instance.PlayMusic("musica_fondo_bucket_llamas");
        UpdateTime(initialTime);
        HideScreen(loseScreen);
        HideScreen(winScreen);
        ballsShooter.UpdateFire(-ballsShooter.actualFires);
        ballsShooter.hose.SetActive(false);
        PoolManager.instance.RestartAssets();
        SetWhiteFade();
        StartCount();
    }
    
    public void UpdateFireText(int number, int totalNumber){
        fireUIText.text = number + "/" + totalNumber;
        UIAnimation.PopImage(fireUIText.transform,0.2f);
        SetTextParticles("+"+1,fireUIText.rectTransform);
    }

    public void ShowTutorial(){

        tutorialStep = 0;
        foreach(CanvasGroup tutScreen in tutorialScreens){
            tutScreen.gameObject.SetActive(false);
        }
        tutorialScreens[0].gameObject.SetActive(true);
        ShowScreen(tutorialScreen);
    }

    public void GoTutorialNext(){
        tutorialStep++;
        if(tutorialStep < tutorialScreens.Count){
            CanvasGroup currentTutorialScreen = tutorialScreens[tutorialStep];
            UIAnimation.FadeOutScreen(tutorialScreens[tutorialStep-1],0.5f,0);
            
            currentTutorialScreen.gameObject.SetActive(false);
            UIAnimation.FadeInScreen(currentTutorialScreen,0.5f);
        }else{
            HideScreen(tutorialScreen);
            StartCount();
        }
    }

}
