﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;

public class CharacterSelectorManager : MonoBehaviour
{

    [Header("Scrolls")]
    public HorizontalScrollSnap hatsScroll;
    public HorizontalScrollSnap clothesScroll;
    public HorizontalScrollSnap vehiclesScroll;

    [Header("Images")]
    public Sprite[] hatsImages;
    public Sprite[] clothesImages;
    public Sprite[] vehiclesImages;

    [Header("Image Prefab")]
    public GameObject imagePrefab;

    [Header("Character Assets")]
    public RunnerCharacterBehaviour character;



    void Start()
    {
        UpdateImages(hatsImages,hatsScroll);
        UpdateImages(clothesImages,clothesScroll);
        UpdateImages(vehiclesImages, vehiclesScroll);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void UpdateImages(Sprite[] imgList,HorizontalScrollSnap scroll){

        GameObject[] array;
        scroll.RemoveAllChildren(out array);

        float initialPointPosX = 0;
        for(int i = 0; i < imgList.Length;i++){
            GameObject image = Instantiate(imagePrefab);
            scroll.AddChild(image);
            image.GetComponent<Image>().sprite = imgList[i];
            image.transform.localScale = new Vector3(1,1,1);

        }
        OnImagePageChanged(scroll);
    }

    public void OnImagePageChanged(HorizontalScrollSnap scroll){

        int currentPage = scroll.CurrentPage;
        scroll.NextButton.SetActive(true);
        scroll.PrevButton.SetActive(true);
        if(currentPage == 3){
            scroll.NextButton.SetActive(false);
        }else if(currentPage <= 0){
            scroll.PrevButton.SetActive(false);
        }

        switch(scroll.gameObject.tag){
            case "HatScroll":
                for(int i = 0; i < character.hats.Length;i++){
                    character.hats[i].gameObject.SetActive(false);
                    if(i == currentPage)
                        character.hats[i].gameObject.SetActive(true);
                }
            break;
            case "ClothScroll":
                character.suitMaterial.mainTexture = character.suitTextures[currentPage];
            break;
            case "VehicleScroll":
                for(int i = 0; i < character.vehicles.Length;i++){
                    character.vehicles[i].gameObject.SetActive(false);
                    if(i == currentPage)
                        character.vehicles[i].gameObject.SetActive(true);
                }
            break;
        }
        SoundManager.instance.Play("click_pieza");
    }
}
