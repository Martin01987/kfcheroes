﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using DG.Tweening;

public class ScreenWipe : MonoBehaviour
{
    [System.NonSerialized]
    public Texture2D tex2D;
    public CanvasGroup screenImage;
    public static ScreenWipe instance;

    float fadeTime = 0.4f;

    [System.NonSerialized]
    public string nextScene;
    // Start is called before the first frame update
    void Start()
    {
        if(instance !=  null){
            Destroy(gameObject);
            return;
        }
        instance = this;
        this.enabled = false;
        DontDestroyOnLoad(this.gameObject);
        screenImage = GetComponent<CanvasGroup>();
        screenImage.alpha = 0;
    }

    public IEnumerator CrossFadeToScene (string sceneName, float time) {
        if (tex2D == null) {
            tex2D = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
        }
        
        yield return new WaitForEndOfFrame();

        //Read the pixels in the Rect starting at 0,0 and ending at the screen's width and height
        tex2D.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0, false);
        tex2D.Apply();
        
        SceneManager.LoadScene(sceneName);
        
        screenImage.alpha = 1;
        this.GetComponent<Image>().sprite = Sprite.Create(tex2D,new Rect(0, 0, tex2D.width, tex2D.height),new Vector2(0.5f, 0.5f));
        screenImage.DOFade(0f,1f).SetDelay(fadeTime);
    }

    public IEnumerator CrossFadeCoroutine (IEnumerator coroutine, float time) {
        if (tex2D == null) {
            tex2D = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
        }
        
        yield return new WaitForEndOfFrame();

        //Read the pixels in the Rect starting at 0,0 and ending at the screen's width and height
        tex2D.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0, false);
        tex2D.Apply();
        
        StartCoroutine(coroutine);
        
        screenImage.alpha = 1;
        this.GetComponent<Image>().sprite = Sprite.Create(tex2D,new Rect(0, 0, tex2D.width, tex2D.height),new Vector2(0.5f, 0.5f));
        screenImage.DOFade(0f,1f).SetDelay(fadeTime);
    }


}
