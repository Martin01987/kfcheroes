﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SplashScreenManager : MonoBehaviour
{
    public float timeToMainMenu = 2.5f;
    // Start is called before the first frame update
    IEnumerator Start()
    {
        SoundManager.instance.PlayMusic("musica_fondo_inicio",1f);
        yield return new WaitForSeconds(timeToMainMenu);
        ScreenWipe.instance.nextScene = "MainMenu";
        StartCoroutine(ScreenWipe.instance.CrossFadeToScene("LoadingScene",1f));
    }

    // Update is called once per frame
    
}
