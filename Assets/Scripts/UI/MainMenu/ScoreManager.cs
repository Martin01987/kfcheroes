﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    public Text lastRecordText;
    public Text scoreText;
    public Image gameIcon;
    public int scoreNumber;
    void OnEnable()
    {
        lastRecordText.text = ""+0;
        scoreText.text = ""+0;
    }

    public void AddPoint(int point){
        scoreNumber+= point;
        scoreText.text = ""+scoreNumber;
    }
}
