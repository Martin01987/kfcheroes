﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LoadingManager : MonoBehaviour
{
    AsyncOperation gameLevel;
    public Text loadingText;
    public Image loadingBar;
    bool checkingProgress;
    Coroutine pointsCoroutine;
    int indexPoint = 0;
    // Start is called before the first frame update
    void Awake()
    {
        StartCoroutine("LoadAsyncOperation");
        pointsCoroutine = StartCoroutine("addPointText");
        loadingText.text = "CARGANDO";
    }

    IEnumerator addPointText(){

        if(indexPoint > 3){
            loadingText.text = "CARGANDO";
            indexPoint = 0;
        }else{
            loadingText.text+=".";
        }
        indexPoint++;
        yield return new WaitForSeconds(0.5f);
        pointsCoroutine = StartCoroutine("addPointText");
    }

    IEnumerator LoadAsyncOperation(){

        yield return new WaitForSeconds(1.4f);
        gameLevel = SceneManager.LoadSceneAsync(ScreenWipe.instance.nextScene);
        gameLevel.allowSceneActivation = false;
        checkingProgress = true;
            
        yield return new WaitForEndOfFrame();
    }

    void Update(){

        if(checkingProgress){
            loadingBar.fillAmount = gameLevel.progress;
            if(gameLevel.progress >= 0.9f){
                loadingBar.fillAmount = 1f;
                checkingProgress = false;
                StartCoroutine(ScreenWipe.instance.CrossFadeCoroutine(ActivateScene(),1f));
                StopCoroutine(pointsCoroutine);
            }
        }
    }

    IEnumerator ActivateScene(){

        yield return new WaitForSeconds(0.5f);
        gameLevel.allowSceneActivation = true;
    }
}

