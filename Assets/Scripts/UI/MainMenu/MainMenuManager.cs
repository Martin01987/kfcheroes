﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

#if PLATFORM_ANDROID
using UnityEngine.Android;
#endif
#if PLATFORM_IOS
using UnityEngine.iOS;
#endif


public class MainMenuManager : MonoBehaviour
{

    [Header("AR Core/ Gyroscope")]
    public bool isLite = true;
    [Header("Panels")]
    public GameObject StartScreen;
    public GameObject PermissionScreen;
    public GameObject NoPermissionScreen;
    public GameObject WarningScreen;
    public Text platformPermissionText; 

    public float animationTime = 1f;

    private GameObject dialog;
    private GameObject actualScreen;
    private GameObject[] screensGame;
    private bool isCheckingCamera;

    List<string> sceneNames = new List<string>{"BucketScene","RunnerScene","QuestionScene"};

    public int getSDKInt() {
        using (var version = new AndroidJavaClass("android.os.Build$VERSION")) {
            return version.GetStatic<int>("SDK_INT");
        }
    }

    void Start()
    {
        Screen.orientation = ScreenOrientation.Portrait;
        //KFCFBAnalyticMgr.instance.LogEvent("Monsters_JuegoIniciado");
        isCheckingCamera = false;
        PermissionScreen.SetActive(false);
        NoPermissionScreen.SetActive(false);

        actualScreen = StartScreen;
        actualScreen.SetActive(true);

        #if PLATFORM_IOS
            platformPermissionText.text = "> Configuración\n> Kids Bucket\n> Permisos";
        KFCFBAnalyticMgr.instance.LogEvent("Monsters_iOS");
        #endif
        #if PLATFORM_ANDROID
            platformPermissionText.text = "> Configuración\n> Aplicaciones\n> KFC Kids Bucket\n> Accesos";
            KFCFBAnalyticMgr.instance.LogEvent("Monsters_ANDROID");
        #endif
    }

    void Awake(){
        //StartCoroutine("CheckARCore");
    }
       
    /*IEnumerator CheckARCore() {

        #if PLATFORM_ANDROID
            int versionNumber = getSDKInt();
            if(versionNumber < 7){
                isLite = true;
            }else{
                isLite = false;
            }
            //Debug.Log("Android version " +  versionNumber);
        #endif

        #if PLATFORM_IOS
            /*int versionNumber = iOS.device.systemVersion;
            Debug.Log(versionNumber + " ios version")
        #endif

        if(!isLite){
            if ((ARSession.state == ARSessionState.None)||(ARSession.state == ARSessionState.CheckingAvailability)){
                yield return ARSession.CheckAvailability();
            }
            if (ARSession.state == ARSessionState.Unsupported){
                isLite = true;
            }
            else{
                isLite = false;
            }
        }
    }*/

// switchs between screens
    public void SwitchScreen(GameObject screen){

        if(screen.name == actualScreen.name)
            return;
        UIAnimation.FadeOutScreen(actualScreen.GetComponent<CanvasGroup>(),animationTime);
        actualScreen = screen;
        UIAnimation.FadeInScreen(actualScreen.GetComponent<CanvasGroup>(),animationTime,animationTime * 0.2f);
    }

//check if camera is available on devices
    public void GoToPermissions(){

        //#if PLATFORM_ANDROID
            if (PermissionsCtrl.Permissions.CheckPermissions()){
                ShowTutorial();
            }else{
                SwitchScreen(PermissionScreen);
            }
        //#endif

        /*#if PLATFORM_IOS
            AskPermission();
        #endif*/
    }

    public void ShowTutorial(){

        SwitchScreen(WarningScreen);        
    }

    public void GoToGame(){

        KFCFBAnalyticMgr.instance.LogEvent("Monsters_PresionaJugar");
        ScreenWipe.instance.nextScene = sceneNames[GameData.instance.gameID];
        
        StartCoroutine(ScreenWipe.instance.CrossFadeToScene("LoadingScene",1f));
    }

// ask permission to use the camera per device
    public void AskPermission(){

        //#if PLATFORM_ANDROID
            /*if (!Permission.HasUserAuthorizedPermission(Permission.Camera))
            {
                Permission.RequestUserPermission(Permission.Camera);
                isCheckingCamera = true;
            }else{
                ShowTutorial();
            }*/
            PermissionsCtrl.Permissions.RequestPermissions(ShowTutorial,GoToNoPermissions);
        //#endif

        /*#if UNITY_IOS
            VerifyiOSCameraPermission();
        #endif*/
    }

    public void GoToNoPermissions(){
        SwitchScreen(NoPermissionScreen);
    }

    public void AskPermissionAgain(){
        SwitchScreen(StartScreen);
    }

    public void SetGameID(int id){
        Debug.Log(id + " id");
        GameData.instance.SetGameID(id);
        if(id == 1)
            GoToGame();
    }

// checks if user granted camera permission in Android
    void OnApplicationFocus(bool hasFocus)
    {
        #if PLATFORM_ANDROID
            if(!isCheckingCamera || !hasFocus)
                return;
            if (!Permission.HasUserAuthorizedPermission(Permission.Camera))
            {
                SwitchScreen(NoPermissionScreen);
                return;
            }
            else{
                ShowTutorial();
            }
            isCheckingCamera = false;
        #endif
    }

// verifies camera permission on iOS
    public void VerifyiOSCameraPermission(){

        if(isLite){
            ShowTutorial();
            return;
        }
        iOSCameraPermission.VerifyPermission(gameObject.name, "iOSCameraCallback");
    }

// callback for iOS Camera permission method
    private void iOSCameraCallback(string permissionWasGranted){
        Debug.Log("Callback.permissionWasGranted = " + permissionWasGranted);
        
        if (permissionWasGranted == "true"){
            ShowTutorial();
        }
        else{
            SwitchScreen(NoPermissionScreen);
        }
    }
}
