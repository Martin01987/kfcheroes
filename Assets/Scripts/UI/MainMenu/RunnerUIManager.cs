﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RunnerUIManager : MonoBehaviour
{
    
    [Header("Panels")]
    public RectTransform instructionsScreen;
    public RectTransform exitScreen;
    public RectTransform characterScreen;

    public RectTransform chickenUI;
    public Text chickenUIText;

    public RectTransform lifeUI;
    public Image lifeBar;

    public CanvasGroup counterGUI;
    public Text counterGUIText;

    public RectTransform winScreen;
    public RectTransform loseScreen;

    [System.NonSerialized]
    public int chickenNumber;

    public static RunnerUIManager instance;

    void Awake(){
        instance = this;
    }

    void Start()
    {
        ActivateUI(false);
        ShowScreen(instructionsScreen);
    }

    void ActivateUI(bool active){
        if(active){
            UIAnimation.PopImage(chickenUI,0.2f,0.2f);
            UIAnimation.PopImage(lifeUI,0.2f);
            lifeBar.fillAmount = 1f;
        }else{
            chickenUI.gameObject.SetActive(false);
            lifeUI.gameObject.SetActive(false);
        }
    }

    public void HideInstructions(){
        HideScreen(instructionsScreen);
        Invoke("ShowSelection",1f);
    }

    void ShowSelection(){
        ShowScreen(characterScreen);
        UIAnimation.PopImage(characterScreen.GetComponent<CharacterSelectorManager>().character.transform,0.2f);
    }

    public void ShowScreen(RectTransform screen){
        UIAnimation.ShowVertical(screen);
    }

    public void HideScreen(RectTransform screen){
        UIAnimation.HideVertical(screen);
    }

    public void StartCount(){
        Invoke("StartCounting",0.5f);
    }

    void StartCounting(){
        StartCoroutine(StartCounter(3));
    }

    IEnumerator StartCounter(int indexCount){

        counterGUIText.text = ""+indexCount;
        UIAnimation.PopImage(counterGUI.transform,0.1f);
        counterGUI.alpha = 1;
        UIAnimation.FadeOutScreen(counterGUI,0.2f,0.5f);
        SoundManager.instance.Play("conteo");
        yield return new WaitForSeconds(1f);

        indexCount--;
        if(indexCount <= 0){
            RunnerGameManager.instance.StartGame();
            ActivateUI(true);
        }else{
            StartCoroutine(StartCounter(indexCount));
        }
    }

    public void StartGame(){
        HideScreen(characterScreen);
        RunnerGameManager.instance.character.gameObject.SetActive(false);
        StartCount();
    }

    public void UpdateBar(int points){
        RunnerGameManager.instance.playerLives+=  points;

        float value = (float)RunnerGameManager.instance.playerLives/5;
        lifeBar.fillAmount = value;

        if(RunnerGameManager.instance.playerLives == 0){
            RunnerGameManager.instance.StopGame();
            ShowScreen(loseScreen);
        }
        //Debug.Log(value + " " + RunnerGameManager.instance.playerLives);
    }

    public void UpdateChicken(int points){
        chickenNumber+= points;
        chickenUIText.text = chickenNumber + " / 10";

        if(chickenNumber >= 10){
            ShowScreen(winScreen);
            RunnerGameManager.instance.StopGame();
        }
    }

    public void SetTextParticles(string text,Vector3 position){

        Text textPart = RunnerPoolManager.instance.GetTextParticles().GetComponent<Text>();
        textPart.rectTransform.localScale = new Vector3(0.8f,0.8f,0.8f);
        textPart.text = text;
        textPart.rectTransform.position = position;
        UIAnimation.ShowFade(textPart.rectTransform);
    }

    public void SetTextParticles(string text, RectTransform transform){
        SetTextParticles(text, transform.position);
    }

    public void GoToMenu(){
        ScreenWipe.instance.nextScene = "MainMenu";
        StartCoroutine(ScreenWipe.instance.CrossFadeToScene("LoadingScene",1f));
    }

    public void RestartGame(){
        ScreenWipe.instance.nextScene = "RunnerScene";
        StartCoroutine(ScreenWipe.instance.CrossFadeToScene("LoadingScene",1f));
    }
}
