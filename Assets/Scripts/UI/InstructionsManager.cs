﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class InstructionsManager : MonoBehaviour
{
    [Header("Instruction Assets")]
    public Text gameNameText;
    public Text explainText;

    public Image instructionsPanel;
    public Image colorPaintImage;
    public Image monsterIcon;
    public Image itemIcon;
    public Image throwArrowImage;

    public int gameID = 2;
    InstructionScreen instructionUsed;

    [Header("Instructions List")]
    public List<InstructionScreen> instructionsDataList;

    [Serializable]
    public class InstructionScreen{

        public string title;
        public string explainText;

        public Color paintColor;
        public Color textColor;

        public Sprite monsterIcon;
        public Sprite colorPanel;
        public Sprite imageItem;

    }

    void Start()
    {
        SetScreen();
    }

    void SetScreen(){

        if(GameData.instance != null)
            gameID = GameData.instance.gameID;

        throwArrowImage.gameObject.SetActive(gameID == 1);
        instructionUsed = instructionsDataList[gameID];

        gameNameText.text = instructionUsed.title.Replace("//","\n");
        gameNameText.color = instructionUsed.textColor;
        explainText.text = instructionUsed.explainText.Replace("//","\n");

        colorPaintImage.color = instructionUsed.paintColor;
        instructionsPanel.sprite = instructionUsed.colorPanel;
        monsterIcon.sprite = instructionUsed.monsterIcon;
        monsterIcon.SetNativeSize();
        itemIcon.sprite = instructionUsed.imageItem;
        itemIcon.SetNativeSize();
    }
}
