﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarManager : MonoBehaviour
{
    // Start is called before the first frame update
    void OnTriggerEnter(Collider collider){
        
        if(collider.CompareTag("Diamond")){
            collider.gameObject.SetActive(false);
            RampGameManager.instance.SetParticles(collider.transform);
            SoundManager.instance.Play("item");
            RampGameManager.instance.RestartGame();
        }
    }
}
