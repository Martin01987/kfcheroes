﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetsManager : MonoBehaviour
{
    [Header("Bucket Targets")]
    public List<TargetBehaviour> FirtsBucketTargets;
    public List<TargetBehaviour> SecondBucketTargets;

    List<TargetBehaviour> usedTargets = new List<TargetBehaviour>();
    public static TargetsManager instance;

    void Awake(){
        instance = this;
        usedTargets = FirtsBucketTargets;
        DeactivateAllTargets();
        
        FirtsBucketTargets[0].isTracked = false;
        SecondBucketTargets[0].isTracked = false;
    }

    public void DeactivateAllTargets(){
        foreach(TargetBehaviour target in FirtsBucketTargets){
            target.isTracked = true;
        }

        foreach(TargetBehaviour target in SecondBucketTargets){
            target.isTracked = true;
        }
    }

    public void DeactivateTargets(int index){

        for(int i = 0; i < usedTargets.Count;i++){
            usedTargets[i].isTracked = true;
        }
        usedTargets[index].isTracked = false;
    }

    public void SetTargets(List<TargetBehaviour> targets){
        usedTargets = targets;
    }
}
