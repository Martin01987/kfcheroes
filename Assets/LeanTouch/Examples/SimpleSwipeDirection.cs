using UnityEngine;
using UnityEngine.UI;

// This script will tell you which direction you swiped in
public class SimpleSwipeDirection : MonoBehaviour
{
	
	protected virtual void OnEnable()
	{
		// Hook into the OnSwipe event
		Lean.LeanTouch.OnFingerSwipe += OnFingerSwipe;
	}
	
	protected virtual void OnDisable()
	{
		// Unhook into the OnSwipe event
		Lean.LeanTouch.OnFingerSwipe -= OnFingerSwipe;
	}
	
	public void OnFingerSwipe(Lean.LeanFinger finger)
	{
		// Store the swipe delta in a temp variable
		var swipe = finger.SwipeDelta;
		
		
		if (swipe.y < -Mathf.Abs(swipe.x))
		{
			RunnerGameManager.instance.MovePlayer(true);
		}
		
		if (swipe.y > Mathf.Abs(swipe.x))
		{
			RunnerGameManager.instance.MovePlayer(false);
		}
	}
}